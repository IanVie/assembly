; asmsyntax=nasm
;*****************************************************************************
;* CS244: Assembler implementation of external functions defined in          *
;*        'tut4.h'.                                                          *
;*****************************************************************************

global fact
global power
global ackerman
global swap
global binary_search

;*****************************************************************************
;* int fact(int n);                                                          *
;*****************************************************************************
%define n [ebp+8]
fact:
  push	ebp
  mov	ebp, esp
  
  mov	ebx, n
  

  cmp	ebx, 0
  je	.if
  mov	eax, n
  dec	eax
  push	eax
  call	fact
  add	esp, 4
  mov	ecx, n
  mul	ecx
  mov	edx, 0
  cmp	edx, 0
  je	.end
.if:
  mov	eax, 1
.end:
  mov	esp, ebp
  pop	ebp
  ret  
;*****************************************************************************
;* int power(int x, int y);                                                  *
;*****************************************************************************
%define y [ebp+12]
%define x [ebp+8]
power:
  push	ebp
  mov	ebp, esp
  
  mov	eax, y
  mov   ecx, x
  

  cmp	eax, 0
  je	.if
  mov	eax, y
  dec	eax
  mov	ecx, x
  push	eax
  push  ecx
  call	power
  add	esp, 8
  mov	ecx, x
  mul	ecx
  jmp	.end
.if:
  mov	eax, 1
.end:
  mov	esp, ebp
  pop	ebp
  ret
;*****************************************************************************
;* int ackerman(int x, int y);                                               *
;*****************************************************************************
%define y [ebp+12]
%define x [ebp+8]
ackerman:
  push	ebp
  mov	ebp, esp
  
  mov	ebx, x
  mov	ecx, y
 
  cmp 	ebx, 0
  jne	.elif
  inc	ecx
  mov	eax, ecx
  jmp	.end

.elif:
  cmp	ecx, 0
  jne	.else
  mov	edx, 1
  dec	ebx
  push	edx
  push  ebx
  call	ackerman
  add	esp, 8
  jmp	.end

.else:
  dec	ecx
  push	ecx
  push	dword x
  call	ackerman
  add	esp, 8
  push	eax
  mov	eax, x
  dec	eax
  push	eax
  call	ackerman
  add	esp, 8 

.end:
  mov	esp, ebp
  pop	ebp  
  ret
;*****************************************************************************
;* void swap(int *x, int *y);                                                *
;*****************************************************************************
%define x_ptr [ebp+8]
%define y_ptr [ebp+12]
swap:
  push  ebp
  mov   ebp, esp
  
  mov   edi, x_ptr
  mov   esi, y_ptr
  
  mov   eax, [edi]
  xchg  eax, [esi]
  xchg  eax, [edi]
  
  mov   esp, ebp
  pop   ebp
  ret
  
;*****************************************************************************
;* int binary_search(int n, int list[], int low, int high);                *
;*****************************************************************************
%define high [ebp+20]
%define low  [ebp+16]
%define list [ebp+12]
%define n    [ebp+8]
%define middle [ebp-4]
binary_search:
  
  push 	ebp
  mov	ebp, esp

  mov	ecx, high
  mov	ebx, low
  mov	eax, middle
  cmp	ebx, ecx 	;if(low>high)
  jg	.if1
  add	ebx, ecx
  mov	eax, ebx
  mov	edx, 0
  mov	edi, 2
  div	edi		;(low+high)/2
  mov	dword middle, eax
  mov	eax, middle
  mov	edx, n
  mov	esi, list
  lea	edi, [esi+eax*4]
  cmp	edx, [edi]
  je	.if2
  jl	.if3
  inc	eax
  mov	dword low, eax
  jmp	.go

.if3:
  dec	eax
  mov	dword high, eax
  jmp	.go
    
.if2:
  mov	eax, middle
  jmp	.end
.if1:
  mov 	eax, -1		;return -1
  jmp	.end
.go:
  push	dword high
  push	dword low
  push  dword list
  push	dword n
  call	binary_search
  add	esp, 16

.end:

  mov  esp, ebp
  pop  ebp 
  ret
