;m test; asmsyntax=nasm
;*****************************************************************************
;* CS244: Assembler implementation of external functions defined in          *
;*        'tut5.h'.                                                          *
;*****************************************************************************

extern free

global binary_sort
global delete_node
global bin_to_string

;*****************************************************************************
;* void binary_sort(node *list, int n);                                      *
;*****************************************************************************
%define list   [ebp+8]
%define n      [ebp+12]
%define i      [ebp-4]
%define j      [ebp-8]
%define bottom [ebp-12]
%define top    [ebp-16]
%define middle [ebp-20]
%define temp   [ebp-56]
binary_sort:
  push  ebp
  mov   ebp, esp
  sub   esp, 88
  
  mov   eax, list
  cmp 	eax, 0		;if (list != NULL)
  je  	.end
  mov  	dword i, 1	;i = 1
  mov 	edx, dword i	;
  cmp 	edx, n		;while(i<n)
  jge   .end
.while1:
  
  mov 	edx, dword i	;
  cmp 	edx, n		;while(i<n)
  jge   .end
  
  mov 	eax, list
  imul  edx, edx, 36
  lea 	esi, [eax+edx]	; temp = list[i]
  mov   edi, ebp
  sub   edi, 56
  
  mov	ecx, 9
  
  rep	movsd

  mov 	dword bottom, 0	; bottom = 0

  mov 	edx, i
  dec 	edx
  mov 	dword top, edx	; top = i-1
  
 .while2:

  mov 	edx, dword top	; while(bottom <= top)
  cmp 	edx, dword bottom
  jl 	.while3
  add  	edx, dword top		; middle = (bottom+top)/2
  shr   edx, 1
  		   	; middle is in eax
  mov   dword middle, edx        ; 
   
  mov   esi, list
  lea   eax, [edx+edx*8]
  mov   eax, [esi+eax*4]
  cmp   dword temp, dword eax		; if(temp.number < list[middle].number)
  jge	.else
  
  dec   edx
  mov   dword top, edx		; top = middle-1
  jmp   .otwhile

.else:
 
  inc 	edx
  mov 	dword bottom, edx		; bottom = middle+1

  
  			
.otwhile:

  jmp .while2

.while3:
 			; j = i -1
  mov 	ebx, dword i
  dec 	ebx
  mov 	dword j, ebx
.while4:
  cmp   ebx, dword bottom		;  while( j >= bottom)
  jl  	.while5
  inc   ebx
  mov   eax, list		; list[j+1] = list[j]
  imul  ebx, ebx, dword 36
  lea   edi, [eax + ebx]
  
  mov   ebx, dword j
  mov   eax, list		;list[j] 
  imul  ebx, ebx, dword 36
  lea   esi,[eax + ebx]
  mov   ecx, 9 
  
  rep   movsd
  mov   ebx, dword j
  dec   ebx		;j = j - 1
  mov   dword j, ebx
  jmp   .while4
  
.while5:

  mov 	edx, dword bottom		;last
  mov 	eax, list
  imul 	edx, edx, dword 36
  lea 	edi, [eax+edx]
  mov 	esi, ebp
  sub 	esi, 56 
  
  mov 	ecx, 9
  
  rep	movsd

  mov 	ebx, dword i
  inc 	ebx
  mov 	dword i, ebx
  jmp 	.while1

  
.end:
  
  mov   esp, ebp
  pop   ebp
  ret

;*****************************************************************************
;* void remove_max(node **root, node **max);                                 *
;*****************************************************************************
%define root [ebp+8]
%define node [ebp+12]
%define max  [ebp-4]
remove_max:
  push  ebp
  mov   ebp, esp
  push  esi
  push  edi
  
  mov 	esi, root
  mov 	esi, [esi]
  mov 	esi, [esi+36]
  mov 	ebx, esi
  cmp 	ebx, 0	; if((*root)->right != NULL) 

  je    .else
  mov   eax, max  ; remove_max_c(*root)->right, max)
  mov   eax, [eax]
  mov   max, eax
  push dword eax 
  push  dword ebx
  call  remove_max
  add   esp, 8
  jmp   .end

.else:
  mov 	esi,root
  mov 	esi,[esi]
  mov 	edi,max
  mov 	[edi],esi
  
  mov 	ebx,[edi]
  mov 	edi,[ebx+32]
  mov 	esi, root
  mov 	[esi], edi 

  
.end: 
  pop 	edi
  pop 	esi
  mov   esp, ebp
  pop   ebp
  ret  

;*****************************************************************************
;* void delete_node(node **root, char *name);                                *
;*****************************************************************************
%define root [ebp + 8]
%define name [ebp + 12]
%define n   [ebp - 4]
%define temp[ebp - 8]
delete_node:
push 	ebp
mov  	ebp, esp
sub 	esp,  8
push 	esi
push 	edi

mov 	eax, root  
cmp 	eax,0 ;if (*root != NULL)
je 		.end2  ; length
	
			
mov 	edi, name
mov		eax, 0	 
mov 	ecx,  32
cld
repnz 	scasb
sub 	ecx, 32
neg 	ecx

			
mov 	esi, name ;if (strcmp(name, (*root)->name) < 0) 
mov 	edi, root
mov 	edi, [edi]
cld
repz 	cmpsb
jge 	.el

			

mov 	eax, name ;delete_node(&(*root)->left, name);
push 	eax
			
mov 	ebx, root ;(*root)->left
mov 	ebx, [ebx]
lea 	edx, [ebx+32]
push 	edx

			
call 	delete_node ;delete_node() now
add 	esp,8
jmp 	.end2



.el:

			
mov 	edi, name ;get length firsth
mov 	ecx, dword 32
cld
repnz 	scasb
sub 	ecx, dword 32
neg 	ecx

			
mov 	esi, name ;if (strcmp(name, (*root)->name) > 0) 
mov 	edi, root
mov 	edi, [edi]
cld
repz 	cmpsb
jle 	.el1

			

mov 	ebx, name		;delete_node_c(&(*root)->left, name);	;&(*root)->left
mov 	eax, root 	;delete_node_c(&(*root)->left, name);
mov 	eax, [eax]
lea 	eax, [eax+36]
push 	ebx
push 	eax
			
call 	delete_node ;delete_node
add 	esp, 8
jmp 	.end2


.el1:

			
mov 	eax, root ; temp = *root
mov 	eax,[eax]
mov 	dword temp, eax

			
mov 	eax, root ;if ((*root)->left == NULL) 
mov 	eax, [eax]
mov 	eax, [eax+32]
cmp 	eax,  0
 
jne 	.el2
			
mov 	eax, root ;(*root)->right;
mov 	eax, [eax]
mov 	eax, [eax+36]

			
mov 	ebx, root ;*root
mov 	[ebx], eax ;*root = (*root)->right

			
jmp 	.end1 


.el2:

			
mov 	eax, root ;if ((*root)->right == NULL) {
mov 	eax, [eax]
mov 	eax, [eax+36]
cmp 	eax,  0

jne 	.el3

mov 	eax, root
mov 	eax, [eax]
mov 	eax, [eax+32]
			
mov 	ebx, root ;*root
mov 	[ebx], eax
			
jmp 	.end1 ;*root = (*root)->left


.el3:

			
		
push 	dword n ;remove_max()
push 	esp    ;n get the value of n


			
mov 	eax, root
mov 	eax, [eax] ;(*root)->left get the left node value
 
lea 	ebx, [eax+32]
push 	ebx
call 	remove_max
pop 	eax
pop 	eax
pop	dword n
		

mov 	ebx, root ;n->left = (*root)->left;
mov 	ebx, [ebx+32]
mov 	ecx, ebx
mov 	eax, n 
mov 	[eax +32], ecx

			

mov 	ebx, root ;n->right = (*root)->right;
mov 	ebx, [ebx]

mov 	edx, [ebx+36]

mov 	eax, n 
mov	[eax+36], edx

mov 	eax, n
mov 	ecx, root
mov 	[ecx], eax

.end1:
	
push 	dword temp
call	free
add 	esp,  4

.end2:

mov 	esp, ebp
pop 	ebp
ret 

  
;*****************************************************************************
;* void bin_to_string(int n, char *s);                                       *
;*****************************************************************************
%define n [ebp+8]
%define s [ebp+12]
%define x [ebp-4]
%define i [ebp-8]

bin_to_string:
  push  ebp
  mov   ebp, esp
  sub	esp, 16
  mov   dword x, 0x80000000	; x = 0x80000000
  mov   dword i, 0		; i = 0
  mov	ecx, i
  cmp	ecx, 32
  jnl	.end
   
.while:
  mov	ecx, i
  cmp	ecx, 32
  jnl	.end
  mov	eax, n
  mov	ecx, x
  and	eax, ecx
  cmp	eax, 0
  je	.else
  mov	eax, i
  mov	edx, s
  add	eax, edx
  mov	byte [eax], 49 ;49
  add	dword i, 1
  shr	dword x, 1
  jmp	.while
  

.else:
  mov	eax, i
  mov	ebx, s
  add	eax, ebx
  mov	byte [eax], 48;s[i] = '0' ; 48
  add	dword i, 1
  shr	dword x, 1
  jmp	.while
  

.end:
  mov 	eax, dword i
  mov 	edx, s
  add	eax, edx
  mov 	byte [eax], 0;0
  mov   esp, ebp
  pop   ebp
  ret 
