; asmsyntax=nasm
;*****************************************************************************
;* CS244: Assembler implementation of external functions defined in          *
;*        'tut1.h'.                                                          *
;*****************************************************************************

global max
global power
global gcd

;*****************************************************************************
;* int max(int x, int y);                                                    *
;*****************************************************************************
%define y [ebp+12]
%define x [ebp+8]
max:
  push  ebp
  mov   ebp, esp
  
  mov   eax, x                     ; if (x > y)
  cmp   eax, y                     ;   return x;
  jg    .else                      ; else
  mov   eax, y                     ;   return y;
                                   
.else:
  mov   esp, ebp
  pop   ebp
  ret

;*****************************************************************************
;* int power(int x, int y);                                                  *
;*****************************************************************************
%define y [ebp+12]
%define x [ebp+8]
%define z [ebp+4]
power:
  push ebp
  mov  ebp, esp

  mov eax, 1
  
  mov 	ebx, y
  cmp 	ebx, 0
  je 	.end
  
.while:
  cmp 	ebx, 0
  je  	.end
  mov 	esi, eax
  mov 	eax, y
  mov 	ecx, 2
  mov 	edx, 0
  div 	ecx
  
  cmp 	eax, 1
  
  mov 	eax, esi
  dec 	ebx
  mov	edx, x
  imul 	eax, edx
  mov	esi, eax

  jmp .while
  

.end:

  mov esp, ebp
  pop ebp
  ret
  
;*****************************************************************************
;* int gcd(int m, int n);                                                    *
;*****************************************************************************
%define n [ebp+12]
%define m [ebp+8]
gcd:
  push	ebp
  mov  	ebp, esp
   
  mov 	ebx, m
  mov	edx, 0
  mov 	ecx, n
.do:
  mov 	eax, ebx
  mov	edx, 0
  div	ecx
  mov	eax, ecx
  cmp	edx, 0
  jne	.if
  je	.while

.if:
  
  mov	ebx, ecx	;m = n
  mov	ecx, edx	;n = r
  mov	eax, ecx
  	
  
  
.while:
  
  cmp	edx, 0 	; while(r !=0)
  jne 	.do
  je	.end
  
.end:

   
  mov 	esp, ebp
  pop 	ebp
  ret
